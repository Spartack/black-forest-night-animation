document.addEventListener("DOMContentLoaded", function () {
  const bg = document.querySelector(".bg");
  for (let index = 0; index < 600; index++) {
    let x = Math.floor(Math.random() * window.innerWidth) + 1;
    let y = Math.floor(Math.random() * window.innerHeight) + 1;
    let size = Math.floor(Math.random() * 3) + 1;
    let duration = Math.random() * 10;

    let star = document.createElement("span");
    star.classList.add("star");
    star.style.top = `${y}px`;
    star.style.left = `${x}px`;
    star.style.width = `${size}px`;
    star.style.height = `${size}px`;
    star.style.animationDuration = `${5 + duration}s`;
    star.style.webkitAnimationDelay = `${duration}s`;

    bg.appendChild(star);
  }
});
